

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;
import java.util.Set;

public class OtherControlTypes {
	public 	WebDriver driver;
	public String baseWebUrl = "https://www.amazon.in/";
	
	
	@BeforeTest
	public void beforemethod() {
		System.setProperty("webdriver.chrome.driver","C:\\Code\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseWebUrl);
	}
	
	@AfterTest
	public void aftermethod() {
		//driver.close();
	}
	/*
	 *Testcase to Click on any Item in Amazon and add to cart 
	 *Code to handle windows switching and accessing elements
	 *Use of text box . 
	 * */

	@Test
	public void AmazonSearchWithWindowsSwitch() {
		
		
		WebElement tabSearchBox = driver.findElement(By.id("twotabsearchtextbox")); 
		//Search for books
		tabSearchBox.sendKeys("books");
		//press Enter button
		tabSearchBox.sendKeys(Keys.RETURN);
		//Verify that correct URL is hit 
		String expectedURL = "https://www.amazon.in/s?k=books&ref=nb_sb_noss";
		

		//Click on Book 
		WebElement desiredBook =  driver.findElement(By.xpath("//span[contains(text(),'The Pursuit of Happiness: A Book of Studies and Strowings')]"));
		desiredBook.click();
		//Wait for add to cart button 
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {}
		// Get current window handle
		String parentWinHandle = driver.getWindowHandle();
		// Get the window handles of all open windows
		Set<String> winHandles = driver.getWindowHandles();
		// Loop through all handles
		for(String handle: winHandles){
			if(!handle.equals(parentWinHandle)){
				driver.switchTo().window(handle);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				System.out.println("Title of the new window: " +
						driver.getTitle());
				System.out.println("Closing the new window...");
				//Click on the button in new window
				WebElement addtoCart = driver.findElement(By.id("add-to-cart-button"));
				addtoCart.click();
			
			}
		}

	}

	/*
	 * Testcase to demostrate the use of dropdown and 
	 * different functions avaliable with dropdowns 
	 * with example.
	 * */
	@Test
	public void DropDownUse() {

		
		WebElement testDropDown = driver.findElement(By.id("searchDropdownBox"));  
		Select dropdown = new Select(testDropDown);  
		//Index is zero based
		//Select by index
		dropdown.selectByIndex(1);
		//Select by value 
		dropdown.selectByValue("search-alias=baby");
		//Select by visible text 
		dropdown.selectByVisibleText("Books");
		//Below commented code wont work as you can only deslect options of a multiselect
		//But the syntax to deselect will work.
		//dropdown.deselectByVisibleText("Books");
		//Select by index
		dropdown.selectByIndex(6);
		

	}
}
