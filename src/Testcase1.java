import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;


/*
 * Write a testcase to verify linked in log page
 * with implementation of TestNG*/

public class Testcase1 {
	
	public 	WebDriver driver;
	public String baseWebUrl = "https://in.linkedin.com/";
	
	/*
	 * Function to be execute before every testMethod*/
	
	@BeforeTest
	public void beforemethod() {
		System.setProperty("webdriver.chrome.driver","C:\\Code\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseWebUrl);
	}

	
	/*
	 * Function to be execute after every testMethod*/
	
	@AfterTest
	public void afterMethod() {
		//Close the browser
		//driver.close();
	
	}
	/*
	 * Test case to verify login credentials of linked in */
	 @Test
	  public void loginTest() {
		 		String expectedWebsiteTitle = "LinkedIn: Log In or Sign Up";
				String actualWebsiteTitle = "";
				
				/* get the actual value of the title*/

				actualWebsiteTitle = driver.getTitle();

				/*
				 * Compare the Website actual title against the expected title
				 * If both titles matches then result is "Passed" else "Failed"
				 */
				//
				if (actualWebsiteTitle.contains(expectedWebsiteTitle)){
					System.out.println("Test Passed!");
				} else {
					System.out.println("Test Failed!");
				}

				//click on sign in Button
				WebElement SigninBtn= driver.findElement((By.xpath("//a[@class='nav__button-secondary']")));
				SigninBtn.click();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				WebElement username = driver.findElement(By.name("session_key"));
				username.click();
				username.sendKeys("test@gmail.com");
				WebElement password = driver.findElement((By.name("session_password")));
				password.sendKeys("test@1");
				WebElement signIn= driver.findElement((By.xpath("//button[@class='btn__primary--large from__button--floating']")));
				signIn.click();
				WebElement errorMsg= driver.findElement((By.id("error-for-username")));
				String errMsgTitle="Hmm, we don't recognize that email. Please try again.";
			
				//Verify the error msg
				if ((errorMsg.getText().equals(errMsgTitle))){
					System.out.println("error msg verified");
					 Assert.assertTrue(true, "error msg verified");
				} else {
					System.out.println("wrong error msg");
					 Assert.assertFalse(false, "error msg is not correct");
				}
				

			}

	    
	   
	  }



